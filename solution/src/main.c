
#include "../include/bmp.h"
#include "./../include/rotate.h"
#include <stdlib.h>

int main( int argc, char** argv ) {

    if (argc != 3 ) {
        fprintf(stderr, "Wrong number of arguments\n");
        exit(1);
    }

    struct image i1 = init_image();
    FILE *input = NULL;
    if (!open_file(&input, argv[1], "rb")) {
        fprintf(stderr, "Open input file error");
        close_file(&input);
        exit(1);
    }

    if (!to_bmp(input, &i1)) {
        fprintf(stderr, "Read error");
        close_file(&input);
        free_image(i1);
        exit(1);
    }
    close_file(&input);
    struct image i2 = rotate_image(i1);
    free_image(i1);

    if (!i2.data) {
        fprintf(stderr, "out of memory");
        free_image(i2);
        exit(1);
    }
    FILE *output = NULL;
    if (!open_file(&output, argv[2], "wb")) {
        fprintf(stderr, "Open output file error");
        close_file(&output);
        free_image(i2);
        exit(1);
    }

    if (!from_bmp(output, &i2)) {
        fprintf(stderr, "Write error");
        free_image(i2);
        close_file(&output);
        exit(1);
    }
    close_file(&output);
    free_image(i2);
    return 0;
}
