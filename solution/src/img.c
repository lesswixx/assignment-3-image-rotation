//
// Created by novik on 30.11.2022.
//
#include "./../include/img.h"
bool open_file(FILE** file, char*  path, char* mode) {
*file = fopen(path, mode);
if (!*file) {
return false;
}
return true;
}

bool close_file(FILE** file) {
    if (!*file) {
        return false;
    }
    fclose(*file);
    return true;
}

struct image init_image(void){
    return (struct image){0};
}

void free_image(struct image img) {
    img.width = 0;
    img.height = 0;
    free(img.data);
}






