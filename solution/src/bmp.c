//
// Created by novik on 30.11.2022.
//
#include "bmp.h"



static uint32_t size_of_pudding(uint32_t width) {
    uint32_t res = width % 4;
    return (res == 0)? 0 : 4-res;
}

//returns false if fail
bool to_bmp(FILE *in, struct image* image) {

    struct bmp_header header;
    if (!fread(&header, sizeof( struct bmp_header ), 1, in ))
        return false;

    *image = (struct image) {header.biWidth, header.biHeight, 0};
    image->data = malloc(image->width * image->height * sizeof(struct pixel));
    if (!image->data) return false;
    uint32_t padding = size_of_pudding(image->width * sizeof(struct pixel));
    for (size_t i = 0; i < image->height; i++) {
        if (fread(image->data + i*image->width, sizeof(struct pixel), image->width, in) != image->width)
            return false;
        if (fseek(in, padding, SEEK_CUR))
            return false;
    }
    return true;
}

static struct bmp_header create_bmp_header(struct image* image, int64_t padding) {
    uint32_t file_size = image->height * (image->width * 3 + padding);
    struct bmp_header header = (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = BF_HEADER_SIZE + file_size,
            .bfReserved = 0,
            .bOffBits = BF_HEADER_SIZE,
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = file_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

bool from_bmp(FILE *out, struct image* image) {

    int64_t padding = size_of_pudding(image->width * sizeof(struct pixel));
    struct bmp_header header = create_bmp_header(image, padding);
    if (header.bfType != BF_TYPE) {
        fclose(out);
        return false;
    }
    if (header.biBitCount != BI_BIT_COUNT) {
        fclose(out);
        return false;
    }

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        fclose(out);
        return false;
    }

    const int32_t zero = 0;
    for (size_t i = 0; i < image->height; i++) {
        if (fwrite(image->data + i*image->width, sizeof(struct pixel), image->width, out) != image->width)
            return false;
        if (fwrite(&zero, 1, padding, out) != padding)
            return false;
    }

    return true;
}
