//
// Created by novik on 30.11.2022.
//
#include "./../include/rotate.h"


static struct pixel get_pixel(struct image img, uint64_t x, uint64_t y) {
    if (x >= img.width || y >= img.height)
        return (struct pixel) {0};
    return img.data[y*img.width + x];
}

static void set_pixel(struct image img, uint64_t x, uint64_t y, struct pixel pixel) {
    if (x >= img.width || y >= img.height)
        return;
    img.data[y*img.width + x] = pixel;
}

struct image rotate_image(struct image img ) {

    struct image ret = {img.height, img.width, 0};
    ret.data = malloc(ret.width * ret.height * sizeof(struct pixel));
    if (!ret.data) return ret;
    for (size_t x = 0; x < img.width; x++) {
        for (size_t y = 0; y < img.height; y++) {
            set_pixel(ret, ret.width - y - 1, x, get_pixel(img, x, y));
        }}
    return ret;
}
