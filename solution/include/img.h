//
// Created by novik on 18.11.2022.
//

#ifndef UNTITLED2_IMG_H
#define UNTITLED2_IMG_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct  __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

bool open_file(FILE** file, char*  path, char* mode);
bool close_file(FILE** file);
struct image init_image(void);
void free_image(struct image img);


#endif //UNTITLED2_IMG_H
