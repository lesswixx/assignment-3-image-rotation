//
// Created by novik on 30.11.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "img.h"

struct image rotate_image(struct image img );

#endif //IMAGE_TRANSFORMER_ROTATE_H
