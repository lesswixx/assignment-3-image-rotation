//
// Created by novik on 18.11.2022.
//

#ifndef UNTITLED2_BMP_H
#define UNTITLED2_BMP_H
#include "img.h"


#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header
{
    FOR_BMP_HEADER( DECLARE_FIELD )
};

enum header_attr {
    BF_TYPE = 19778,
    BI_BIT_COUNT = 24,
    BF_HEADER_SIZE = 54,
    BI_SIZE = 40,
    BI_PLANES = 1
};

bool to_bmp(FILE *in, struct image* image);
bool from_bmp(FILE *out, struct image* image);
#endif //UNTITLED2_BMP_H
